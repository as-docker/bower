FROM library/node:5.0
MAINTAINER Tomasz Rzany <tomasz.rzany@amsterdam-standard.pl>

# Install requirements.
RUN npm install -g bower
RUN echo '{ "allow_root": true }' > /root/.bowerrc

# For some strange reason Bower chowns files after instalation and fails to do so when running as root.
# Running as www-data in directory owned by www-dara fix the problem
RUN mkdir -p /data/www /var/www && chown www-data:www-data /var/www
VOLUME ["/data"]
WORKDIR /data/www

USER www-data

ENTRYPOINT ["bower"]
CMD ["help"]
