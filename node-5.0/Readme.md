# About this image

This image is based on node [node](https://hub.docker.com/_/node/) image.
It is extended to support bower.

# Docker-compose example

```
pushd:
    image: amsdard/bower:latest
    command: install
    volumes:
      - ./:/data/www


```
Use /data/www volume to attach your app with bower.json file.


